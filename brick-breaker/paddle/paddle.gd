extends KinematicBody2D

onready var sprite = $Sprite

export var speed: float = 500
export var ball_start_pos: Vector2 = Vector2(20, -30)

func fixed_time() -> float:
    return (OS.get_ticks_msec() % 3_600_000) / 1000.0

func _process(_delta: float) -> void:
    (sprite.material as ShaderMaterial).set_shader_param("outside_time", fixed_time())

func _physics_process(delta: float) -> void:
    var mouse_x = get_viewport().get_mouse_position().x
    var self_x = (get_viewport_transform() * self.position).x

    if mouse_x > self_x:
        self.move_and_collide(min(speed * delta, mouse_x - self_x) * Vector2.RIGHT)
    elif mouse_x < self_x:
        self.move_and_collide(min(speed * delta, self_x - mouse_x) * Vector2.LEFT)

func _on_ball_hit(collision_pos: Vector2) -> void:
    var sprite_material = sprite.material as ShaderMaterial
    sprite_material.set_shader_param("hit_time",  fixed_time())

    var relative_pos = (collision_pos - self.position) / sprite.texture.get_size() + Vector2(0.5, 0.5)
    sprite_material.set_shader_param("hit_point", relative_pos)
