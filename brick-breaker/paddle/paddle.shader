shader_type canvas_item;

uniform vec4 wave_color: hint_color = vec4(1);
uniform vec2 hit_point = vec2(0);
uniform float outside_time = 0;
uniform float hit_time = 0;
uniform float wave_speed = 2;
uniform float wave_hardness = 4;
uniform bool repeat = false;
uniform bool use_outside_time = true;

void fragment() {
    vec4 default_color = texture(TEXTURE, UV);

    float normalized_dist = distance(vec2(0.), 1. / TEXTURE_PIXEL_SIZE.xy);
    float dist_ratio = distance(hit_point / TEXTURE_PIXEL_SIZE.xy, UV.xy / TEXTURE_PIXEL_SIZE.xy) / normalized_dist;
    
    float normalized_time = ((use_outside_time ? outside_time : TIME) - hit_time) * wave_speed;
    if (repeat) {
        normalized_time = mod(normalized_time, 1.2);    
    }
    
    float ratio = pow(dist_ratio/normalized_time, wave_hardness);

    float coef = 1.;
    if (ratio <= 3.14) {
        coef = cos(ratio);
    }
    
    COLOR.rgb = mix( min(vec4(1.2), wave_color + default_color), default_color, coef).rgb;
    COLOR.a = default_color.a;
}