extends KinematicBody2D

const START_VELOCITY = 500
var velocity: Vector2 = Vector2.ZERO
var pinned_object: Node2D
var pinned_offset = Vector2.ZERO
var pinned = false

const particles_model = preload("res://particles/hit_particles.tscn")

func _physics_process(delta: float) -> void:
    if pinned:
        self.position = pinned_object.position + pinned_offset
    else:
        var collision = self.move_and_collide(velocity * delta)
        if collision:
            resolve_collision(collision)


func pin_to(obj: Node2D) -> void:
    pinned_offset = self.position - obj.position
    pinned_object = obj
    pinned = true

func _input(event: InputEvent) -> void:
    if pinned and (event.is_action_pressed("launch_ball") or
    event is InputEventScreenTouch and not (event as InputEventScreenTouch).pressed):
        velocity = pinned_offset.normalized() * START_VELOCITY
        pinned = false

func resolve_collision(collision: KinematicCollision2D) -> void:
    var particles = particles_model.instance()
    particles.emitting = true
    particles.position = collision.position
    particles.direction = collision.normal
    get_parent().add_child(particles)

    if collision.collider.has_method("_on_ball_hit"):
        collision.collider._on_ball_hit(collision.position)

    # Bounce
    velocity = velocity.bounce((collision.normal + collision.collider_velocity.normalized() * 0.25).normalized())

    var new_collision = self.move_and_collide(collision.remainder.length() * velocity.normalized())
    if new_collision:
        print_debug("double collision")
        pass
