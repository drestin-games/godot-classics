extends Node2D

export var ball_count = 3 setget set_ball_count

const ball_model = preload("res://ball/ball.tscn")
onready var paddle = $Paddle

const BALL_GROUP = "balls"

signal ball_count_changed(new_ball_count)

func _ready() -> void:
    set_ball_count(ball_count)
    new_ball()

func new_ball() -> void:
    var ball = ball_model.instance()
    ball.set_position(paddle.position + paddle.ball_start_pos)
    ball.pin_to(paddle)
    ball.add_to_group(BALL_GROUP)
    self.add_child(ball)

func set_ball_count(new_ball_count: int) -> void:
    ball_count = new_ball_count
    self.emit_signal("ball_count_changed", new_ball_count)


func _on_Pit_body_entered(body: Node) -> void:
    if body.is_in_group(BALL_GROUP):
        body.free()
        set_ball_count(ball_count - 1)
        new_ball()
