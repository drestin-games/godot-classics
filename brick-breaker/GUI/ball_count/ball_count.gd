extends Control

onready var label = $Label

func _on_ball_count_changed(new_ball_count: int) -> void:
    if label:
        label.text = "%02d" % new_ball_count
