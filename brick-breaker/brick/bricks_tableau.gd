extends Node2D

tool

export var width = 10 setget set_width
export var height = 5 setget set_height

export var offset = Vector2(200, 50) setget set_offset

const brick_model = preload("res://brick/brick.tscn")

func _ready() -> void:
    recreate_bricks()

func set_height(h: int) -> void:
    height = h
    recreate_bricks()

func set_width(w: int) -> void:
    width = w
    recreate_bricks()

func set_offset(o: Vector2) -> void:
    offset = o
    recreate_bricks()

func recreate_bricks() -> void:
    for brick in self.get_children():
        brick.queue_free()

    for x in range(width):
        for y in range(height):
            var new_brick = brick_model.instance()
            new_brick.translate(Vector2(x + 0.5, y + 0.5) * offset)
            add_child(new_brick)

