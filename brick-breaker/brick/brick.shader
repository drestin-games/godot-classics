shader_type canvas_item;

uniform vec4 wave_color: hint_color = vec4(1);
uniform float wave_speed = 2;
uniform float wave_hardness = 4;

uniform sampler2D noise_texture;

uniform vec2 hit_point = vec2(0);
uniform float hit_time = 0;
uniform float outside_time = 0;

// To ease testing
uniform bool repeat = false;
uniform bool use_outside_time = true;
uniform bool enabled = false;

void fragment() {
    
    vec4 default_color = texture(TEXTURE, UV);

    float max_dist = distance(vec2(0.), 1. / TEXTURE_PIXEL_SIZE.xy);
    float normalized_dist = distance(hit_point / TEXTURE_PIXEL_SIZE.xy, UV.xy / TEXTURE_PIXEL_SIZE.xy) / max_dist;
    
    float normalized_time = ((use_outside_time ? outside_time : TIME) - hit_time) * wave_speed;
    if (repeat) {
        normalized_time = mod(normalized_time, 2);    
    }
    
    float noise = texture(noise_texture, UV).r;
    float ratio = pow(normalized_dist/normalized_time, wave_hardness) + noise - 1.;

    vec4 burn_color = min(vec4(2), 2. * wave_color + default_color);
    
    if (ratio >= 1. || !enabled) {
        COLOR = default_color;    
    } else if (ratio <= 0.5) {
        COLOR.rgb = burn_color.rgb;

        COLOR.a = max(0, sin(ratio * 3.14) * 1.2 - 0.2);
    } else {
        COLOR.rgb = mix( burn_color, default_color, 1.-sin(ratio * 3.14)).rgb;
        COLOR.a = default_color.a;
    } 
    
}