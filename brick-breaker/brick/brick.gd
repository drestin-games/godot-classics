extends StaticBody2D

const break_particles_model = preload("res://brick/break_particles.tscn")
onready var collision_shape = $CollisionShape2D
onready var free_timer = $FreeTimer
onready var sprite = $Sprite

func fixed_time() -> float:
    return (OS.get_ticks_msec() % 3_600_000) / 1000.0

func _process(_delta: float) -> void:
    (sprite.material as ShaderMaterial).set_shader_param("outside_time", fixed_time())

func _on_ball_hit(collision_pos: Vector2) -> void:
    var particle = break_particles_model.instance()
    particle.position = self.position

    #particle.emission_rect_extents = (collision_shape.shape as RectangleShape2D).extents * collision_shape.scale
    #particle.emitting = true
    #get_parent().add_child(particle)
    collision_shape.disabled = true
    free_timer.start()

    var sprite_material = sprite.material as ShaderMaterial
    sprite_material.set_shader_param("hit_time",  fixed_time())

    var relative_pos = (collision_pos - self.position) / sprite.texture.get_size() + Vector2(0.5, 0.5)
    sprite_material.set_shader_param("hit_point", relative_pos)

    sprite_material.set_shader_param("enabled", true)

func _on_FreeTimer_timeout() -> void:
    self.queue_free()
